package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"

	entity "bitbucket.org/test-go-mustache/entity"
	"github.com/cbroglie/mustache"
	structs "github.com/fatih/structs"
	"github.com/gorilla/mux"
)

type ControllerTest struct {
}

func (controllerTest *ControllerTest) Test(w http.ResponseWriter, r *http.Request) {

	log.Println(fmt.Sprintf("########### ControllerTest: %v ###########", ">>>>>"))
	vars := mux.Vars(r)

	name := vars["name"]
	log.Println(fmt.Sprintf("########### Name: %v ###########", name))

	person := entity.Person{name}
	personJSON, _ := json.Marshal(person)
	log.Println(fmt.Sprintf("########### personJSON: %v ###########", string(personJSON)))

	filename := path.Join(path.Join(os.Getenv("PWD"), "layout"), "home.html.mustache")

	log.Println(fmt.Sprintf("########### filename: %v ###########", filename))
	tmpl, err := mustache.ParseFile(filename)

	if err != nil {
		log.Printf("err: %s", err)
	}
	log.Println(fmt.Sprintf("########### tmpl: %v ###########", tmpl))
	
	tmpl.FRender(w, structs.Map(person))

}