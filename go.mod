module bitbucket.org/test-go-mustache

go 1.13

require (
	github.com/cbroglie/mustache v1.0.1
	github.com/fatih/structs v1.1.0
	github.com/gorilla/mux v1.7.4
	github.com/kelseyhightower/envconfig v1.4.0
	gopkg.in/yaml.v2 v2.2.8
)
